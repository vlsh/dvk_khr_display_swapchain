#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

#[macro_use]
extern crate libc;
extern crate shared_library;
#[macro_use]
extern crate dvk;
extern crate dvk_khr_swapchain;

use self::libc::{c_void, c_char, uint32_t, size_t, uint64_t, c_float, int32_t, uint8_t};
use self::shared_library::dynamic_library::DynamicLibrary;
use std::path::{Path};
use std::ffi::CString;
use dvk::*;
use dvk_khr_swapchain::*;

pub const VK_KHR_DISPLAY_SWAPCHAIN_SPEC_VERSION: uint32_t = 9;
pub const VK_KHR_DISPLAY_SWAPCHAIN_EXTENSION_NAME: *const c_char = b"VK_KHR_display_swapchain\0" as *const u8 as *const c_char;

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrDisplaySwapchainResult {
    VK_SUCCESS = 0,
    //VK_NOT_READY = 1,
    //VK_TIMEOUT = 2,
    //VK_EVENT_SET = 3,
    //VK_EVENT_RESET = 4,
    //VK_INCOMPLETE = 5,
    VK_ERROR_OUT_OF_HOST_MEMORY = -1,
    VK_ERROR_OUT_OF_DEVICE_MEMORY = -2,
    //VK_ERROR_INITIALIZATION_FAILED = -3,
    VK_ERROR_DEVICE_LOST = -4,
    //VK_ERROR_MEMORY_MAP_FAILED = -5,
    //VK_ERROR_LAYER_NOT_PRESENT = -6,
    //VK_ERROR_EXTENSION_NOT_PRESENT = -7,
    //VK_ERROR_FEATURE_NOT_PRESENT = -8,
    //VK_ERROR_INCOMPATIBLE_DRIVER = -9,
    //VK_ERROR_TOO_MANY_OBJECTS = -10,
    //VK_ERROR_FORMAT_NOT_SUPPORTED = -11,
    VK_ERROR_SURFACE_LOST_KHR = -1000000000,
    //VK_ERROR_NATIVE_WINDOW_IN_USE_KHR = -1000000001,
    //VK_SUBOPTIMAL_KHR = 1000001003,
    //VK_ERROR_OUT_OF_DATE_KHR = -1000001004,
    VK_ERROR_INCOMPATIBLE_DISPLAY_KHR = -1000003001,
    //VK_ERROR_VALIDATION_FAILED_EXT = -1000011001,
    //VK_ERROR_INVALID_SHADER_NV = -1000012000
}

#[repr(u32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrDisplaySwapchainStructureType {
    VK_STRUCTURE_TYPE_DISPLAY_PRESENT_INFO_KHR = 1000003000
}

#[repr(C)]
pub struct VkDisplayPresentInfoKHR {
    pub sType: VkKhrDisplaySwapchainStructureType,
    pub pNext: *const c_void,
    pub srcRect: VkRect2D,
    pub dstRect: VkRect2D,
    pub persistent: VkBool32
}

pub type vkCreateSharedSwapchainsKHRFn = unsafe extern "stdcall" fn(device: VkDevice,
                                                                    swapchainCount: uint32_t,
                                                                    pCreateInfos: *const VkSwapchainCreateInfoKHR,
                                                                    pAllocator: *const VkAllocationCallbacks,
                                                                    pSwapchains: *mut VkSwapchainKHR) -> VkKhrDisplaySwapchainResult;

#[cfg(windows)]
static VULKAN_LIBRARY: &'static str = "vulkan-1.dll";

#[cfg(unix)]
static VULKAN_LIBRARY: &'static str = "libvulkan-1.so";

#[derive(Default)]
pub struct VulkanKhrDisplaySwapchain {
    library: Option<DynamicLibrary>,
    vkGetInstanceProcAddr: Option<vkGetInstanceProcAddrFn>,
    vkCreateSharedSwapchainsKHR: Option<vkCreateSharedSwapchainsKHRFn>,
}

impl VulkanKhrDisplaySwapchain {
    pub fn new() -> Result<VulkanKhrDisplaySwapchain, String> {
        let mut vulkan_khr_display_swapchain: VulkanKhrDisplaySwapchain = Default::default();
        let library_path = Path::new(VULKAN_LIBRARY);
        vulkan_khr_display_swapchain.library = match DynamicLibrary::open(Some(library_path)) {
            Err(error) => return Err(format!("Failed to load {}: {}",VULKAN_LIBRARY,error)),
            Ok(library) => Some(library),
        };
        unsafe {
            vulkan_khr_display_swapchain.vkGetInstanceProcAddr = Some(std::mem::transmute(try!(vulkan_khr_display_swapchain.library.as_ref().unwrap().symbol::<u8>("vkGetInstanceProcAddr"))));
        }
        Ok(vulkan_khr_display_swapchain)
    }

    unsafe fn load_command(&self, instance: VkInstance, name: &str) -> Result<vkVoidFunctionFn, String> {
        let fn_ptr = (self.vkGetInstanceProcAddr.as_ref().unwrap())(instance, CString::new(name).unwrap().as_ptr());
        if fn_ptr != std::ptr::null() {
            Ok(fn_ptr)
        } else {
            Err(format!("Failed to load {}",name))
        }
    }

    pub fn load(&mut self, instance: VkInstance) -> Result<(), String> {
        unsafe {
            self.vkCreateSharedSwapchainsKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkCreateSharedSwapchainsKHR"))));
        }
        Ok(())
    }

    pub unsafe fn vkCreateSharedSwapchainsKHR(&self, 
                                              device: VkDevice,
                                              swapchainCount: uint32_t,
                                              pCreateInfos: *const VkSwapchainCreateInfoKHR,
                                              pAllocator: *const VkAllocationCallbacks,
                                              pSwapchains: *mut VkSwapchainKHR) -> VkKhrDisplaySwapchainResult {
        (self.vkCreateSharedSwapchainsKHR.as_ref().unwrap())(device, swapchainCount, pCreateInfos, pAllocator, pSwapchains)
    }
}